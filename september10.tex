\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 10 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}

\begin{document}

\maketitle

\section*{Highlights}
\begin{enumerate}
  \item Why can we construct a 17-gon but not a 7-gon using a straightedge and
  compass?
  \item Why can we solve certain (general) polynomial equations (e.g. quadratics),
   but not others (e.g. quintics) using just algebraic operations
  \item Why can't we solve certain integrals using only elementary functions?
\end{enumerate}

If there are no other questions, I want to start with group theory,
which will occupy us until maybe the first term test

\section*{Group Theory}

In one phrase, group theory can be described as ``the study of symmetries.''
Maybe this already tells you that it is very prevalent in other parts of math and
in applications, since symmetries arise everywhere in nature.

Examples include conservation laws in physics (Noether's theorem), various symmetric
objects in geometry, symmetries in analysis, leading to Fourier theory, and later on
we will see that they arise in algebra as well, with Galois theory as an example.

Group theory arose in different contexts at different times, but it took a long time
for mathematicians to isolate the proper axioms and develop the abstract concept of
a group, which was only introduced after 1850. I'll post a link to one of the earliest
papers of Cayley where he introduces the concept of an abstract group (as some sort of
symmetry of functions).

Before talking about the group as an abstract concept, I want to give two concrete
examples of a group in mathematics:
\begin{itemize}

  \item Symmetries of \(n\) (identical) objects: if we have objects labelled from
  1 to \(n\), how many different ways can we rearrange them i.e. the permutations
  of \(1,...,n\), or the bijections of \(\{1,...,n\}\) to itself.

  Later we'll be calling this \(S_n\). Note \(\#S_n = n!\).

  Let's begin by looking at \(S_4\). An example of a permutation in \(S_4\) is
  the function which maps \(1\) to \(4\), \(2\) to \(1\), \(3\) to \(2\)
  and \(4\) to \(3\).

  If we have two bijections (permutations) and compose them, we get another one.
  Furthermore, the inversion of a permutation is also a permutation.

  We can use this to obtain some operations to create new symmetries out of old ones:
  \begin{itemize}
    \item Composition (of functions)
    \item Inversion (of functions)
  \end{itemize}

  Now, what properties do these operations satisfy?
  \begin{itemize}
      \item There is an identity function \(id\) which maps every number to itself
      \item \(id \circ f = f \circ id\)
      \item \(\forall\) permutations \(f\), \(\exists f^{-1}\),
      \(f^{-1} \circ f = f \circ f^{-1} = id\)
      \item Associativity: \((f_1 \circ f_2) \circ f_3 = f_1 \circ (f_2 \circ f_3)\)
  \end{itemize}

  There are maybe other laws we can find in this particular case, but these are going
  to play the basic role in the future.

  \item Now, let's briefly look at an example of symmetries from geometry: the
  rotational symmetries of a cube. By this I mean every kind of rotation of \(\reals^3\)
  that preserves our cube, say the unit cube.

  For example, we can rotate it by (any multiple of) \(\frac{\pi}{4}\) around
  any face, or rotate it by (any multiple of) \(\frac{2\pi}{3}\) around the
  ``space diagonals''  between opposite vertices (since looking down along one,
  we see a triangle). We can also rotate (any multiple of) \(\pi\) around the axis
  between the midpoints of opposing sides.

  With some work, we can show that any cube-preserving rotation of \(\reals^3\) can
  be written in terms of these
  \[8 \times 3 = 6 \times 4 = 12 \times 2 = 24\]
  rotational symmetries (8 vertices, 3 rotations, 6 faces, 4 rotations or
  12 edges, 2 rotations).

  All these symmetries are bijections from the cube to itself, so we can see that
  the rules we derived above for permutations still hold.

  A final thing I want to say about this example is that, in a way, both of these
  are simple examples of groups. But, in a way, these two examples are the same:
  note again that \(\#S_4 = 24\).

  Let me try to convince you that these two groups represent the same kind of
  symmetries: each rotation is just creating a permutation of the vertices on the
  cube where opposite vertices are labelled the same (since they must remain opposites).
  Alternatively, we can just label the space diagonals between vertices.

\end{itemize}

\subsection*{Definitions}

\begin{definition}
  A \underline{binary operation} \(*\) on a set \(G\) is a function
  \[*: G \times G \to G, (x, y) \mapsto x * y\]
  There are many many examples of binary operations:
  \begin{itemize}
    \item Compositions on the set \(S_n\)
    \item \(+, -, \times\) on any field
    \item \(/\) on \(K - \{0\}\) where \(K\) is any field
    \item \(+, -, \times\) on matrices
    \item Function composition on the set of functions from \(X \to X\).
  \end{itemize}
\end{definition}

\begin{definition}
  A \underline{group} is a set \(G\) together with a binary operation \(*\) such
  that
  \begin{enumerate}
    \item Associativity: \(\forall x, y, z \in G, x * (y * z) = (x * y) * z\)
    \item There exists an \underline{identity} \(e \in G\) such that
    \(\forall x \in G, e * x = x * e = x\)
    \item Every \(x \in G\) has an \underline{inverse} \(x^{-1} \in G\) such that
    \(x * x^{-1} = x^{-1} * x = e\).
  \end{enumerate}
\end{definition}

Examples:
\begin{itemize}
  \item \((S_n, \circ)\) forms a group
  \item The set of symmetries of the cube form a group
  \item \((\ints, +)\), the integers over addition, forms a group, but
  \((\ints, \times)\) does not (since there exist elements, such as \(2\), without
  an inverse). Similarly \((\nats, +)\) is not a group, since, for example, \(1\)
  does not have an inverse.

  Note in this example \(e = 0, x^{-1} = -x\). We have to be a little bit careful
  what symbols we want to write down.
  \item \((\reals \setminus \{0\}, \times)\), or more generally
  \((K \setminus \{0\}, \times)\) where \(K\) is any field
  \item For any vector space \(V\), \((V, +)\) is a group
  \item For any field \(K\), \((K, +)\) is a group
\end{itemize}

\end{document}
