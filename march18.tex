\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 11 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Deg}{deg}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Ev}{ev}
\DeclareMathOperator{\Char}{char}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Stab}{Stab}
\DeclareMathOperator{\Orb}{Orb}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Recall that a finite extension \(K/F\) is called a normal extension if it is the splitting field of some polynomial \(f(x) \in F[x]\), which is true if and only if any irreducible polynomial \(f(x) \in F[x]\) that has one root in \(K\) splits over \(K\).
\begin{definition}
  \(f(x) \in F[x]\) is \underline{separable} if it has no repeated roots in a splitting field \(K\), i.e.
  \begin{equation}
    f(x) = c\prod_{i = 1}^n(x - \alpha_i)
  \end{equation}
  where \(\alpha_i \in K\) are \underline{distinct}. Otherwise, it is called \underline{inseparable}.
  \end{definition}
Remark that, in the field \(F = \field{p}(t)\), the polynomial \(x^p - t\)
is inseparable and irreducible. Over the splitting field, however, it can be written as \((x - t^{1/p})^p\)
\begin{lemma}
  If \(\Char(F) = p\), then \(\sigma_p: F \to F, x \mapsto x^p\) is a field homomorphism. Specifically, it is called the \underline{Frobenius homomorphism}.
\end{lemma}
Note that \(\sigma_p\) is injective but not always surjective.
\begin{definition}
  \(\alpha \in K\) is \underline{separable over \(F\)} if its minimal polynomial is separable. Then we say that an algebraic extension \(K/F\) is \underline{separable} if every \(\alpha \in K\) is separable.
\end{definition}
This is a really inconvenient definition, since in general we won't check everything for every element of \(K\). But soon we will find some useful facts about separability which will simplify matters.
\begin{proposition}
Let \(f(x) \in F[x]\). Then
\begin{enumerate}[label=(\roman*)]

  \item \(\alpha \in K\) is a multiple root of \(f\) if and only if \(\alpha\) is a root of \(f\) and \(Df\). Here \(Df\) is the \underline{formal derivative}, defined such that
  \begin{equation}
    f(x) = \sum_{i = 0}^na_ix^i \implies Df = \sum_{i = 1}^nia_ix^{i - 1}
  \end{equation}
  considering \(i \in \nats\) acting on \(F\) in the natural manner. Properties of the derivative are the expected ones:
  \begin{equation}
    \forall f, g \in F[x], \lambda \in F, D(\lambda f + g) = \lambda Df + Dg, D(fg) = (Df)g + (Dg)f, f \neq 0 \implies \deg Df < \deg f
  \end{equation}

  \item \(f\) is a separable polynomial if and only if \(f, Df\) are relatively prime in \(F[x]\)

  \item If \(f(x)\) is irreducible, then \(f(x)\) is inseparable if and only if \(Df = 0\)

\end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}[label=(\roman*)]

    \item \(f(x) = (x - \alpha)^kg(x)\) such that \((x - \alpha) \nmid g(x)\). We have
    \begin{equation}
      (Df)(x) = (x - \alpha)^kD(g) + g(x)D((x - \alpha)^k) = (x - \alpha)^kD(g) + g(x)k(x - \alpha)^{k - 1}
    \end{equation}
    Hence,
    \begin{equation}
      Df(\alpha) = \left\{\begin{array}{cc}
        0 & \text{if } k \geq 2 \\
        g(\alpha) \neq 0 & \text{if } k = 1
      \end{array}\right.
    \end{equation}
    So \((Df)(\alpha) = 0 \iff k \geq 2 \iff x\) is a multiple root of \(f\) \label{first}

    \item We remark that if \(R \subset S\) are PIDs, then \(\gcd(a, b) \in R\) is the same in \(R, S\) up to units (exercise). By \ref{first}, \(f\) is inseparable if and only if \(f, Df\) have a common root (common factor \(x - \alpha\)) in a splitting field \(K\), which is true if and only if \(\gcd(f, Df) \neq 1\). \label{second}

    \item By \ref{second}, \(f\) is inseparable if and only if \(\gcd(f, Df) \neq 1\). But since \(f\) is irreducible, \(f | Df\), but since the derivative has smaller degree than \(f\), it follows that \(Df = 0\).

  \end{enumerate}
\end{proof}
We now need to analyze what it means for \(Df = 0\).
\begin{proposition}
  Let \(f(x) \in F[x]\) be such that \(Df = 0\).
  \begin{enumerate}[label=(\roman*)]

    \item If \(\Char F = 0\), then \(f(x) = c \in F\)

    \item If \(\Char F = p\), then \(f(x) = g(x^p)\) for some \(g(x) \in F[x]\)

  \end{enumerate}
  Note that this can be written in general as \(f(x) = g(x^{\Char F})\) for some \(g(x) \in F[x]\), since \(g(x^0) = c \in F\).
\end{proposition}
\begin{proof}
  We have
  \begin{equation}
    f(x) = \sum a_nx^n \implies Df = \sum na_nx^{n - 1} = 0 \iff \forall n, na_n = 0
  \end{equation}
  We now split our proof into cases:
  \begin{enumerate}[label=(\roman*)]

    \item If \(\Char F = 0\), we have \(n > 0 \implies n \neq 0 \implies a_n = 0 \forall n > 0\).

    \item If \(\Char F = p \neq 0\), \(n = 0\) in \(F\) if and only if \(p|n\), so
    \begin{equation}
      \forall p \nmid n, a_n = 0 \implies f(x) = a_0 + a_px^p + a_{2p}x^{2p} + ...
    \end{equation}

  \end{enumerate}
\end{proof}
\begin{corollary}
  If \(\Char(F) = 0\), then \underline{any} algebraic extension \(K/F\) is separable
\end{corollary}
\begin{proof}
  The minimal polynomial \(m_{\alpha, F}\) of \(\alpha \in K\) is always an irreducible polynomial, implying that \(Dm_{\alpha, F} = 0\). But if \(\Char F = 0\), then \(m_{\alpha, F}\) is constant.
\end{proof}
We can generalize this previous corollary as follows:
\begin{definition}
  A field \(F\) is \underline{perfect} if any algebraic extension \(K/F\) is separable.
\end{definition}
\begin{theorem}
  If \(\Char(F) = p\), then \(F\) is perfect if and only if the Frobenius homomorphism \(\sigma_p\) is an isomorphism, which is true if and only if \(F^p = F\)
\end{theorem}
\begin{proof}
We'll prove one direction this time:
  \begin{itemize}

    \item [\(\impliedby\):] if \(\alpha\) is \underline{not} separable, then by the propositions above
    \begin{equation}
      D(m_{\alpha, F}) = 0 \implies m_{\alpha, F} = g(x^p) = \sum a_n^{1/p}x^{np} = \left(\sum a_n^{1/p}x^n \right)^p
    \end{equation}
    This gives a contradiction, since this would mean that \(m_{\alpha, F}\) is not irreducible (since it is the \(p^{th}\) power of another polynomial).


  \end{itemize}
\end{proof}


\end{document}
