\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 17 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Stab}{Stab}
\DeclareMathOperator{\ccl}{ccl}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Last time:
\begin{itemize}

  \item \(G \acts A \iff G \xrightarrow[homo]{} S_A\)

  \item \(\Stab_G(a) \leq G\)

  \item \(Ga \subset A\), the orbit of \(a\)

  \item \(|Ga||\Stab_G(a)| = |G|\)

\end{itemize}
\begin{theorem}[Cayley]
  Any group \(G\) is isomorphic to a subgroup of \(S_G\).
\end{theorem}
\begin{proof}
  \(G \acts G\) by multiplication on the left: \(g \cdot a = ga\). Consider the associated permutation representation
  \[\varphi: G \xrightarrow[homo]{} S_G\]
  We have to check that this homomorphism is an injection \(\iff \ker\varphi = 1\), which, implying \(G \simeq \Ima\varphi \leq S_G\).

  We have
  \[\varphi: G \to S_G, g \mapsto (h \mapsto g \cdot h = gh)\]
  Hence
  \[g \in \ker\phi \iff h \mapsto gh \text{ is the identity permutation } \iff [\forall h, gh = h] \iff g = 1\]
\end{proof}
Remark: We also have a group action \(G \acts G / H\) by left multiplication for \(H \leq G\), with
\[g \cdot xH = gxH\]
Recall the following definitions: if \(G\) is a group,
\begin{itemize}

  \item For \(g \in G\), the \textit{centralizer} of \(g\) in \(G\) is given by
  \[C_G(g) := \{a \in G | ag = ga\}\]

  \item For \(S \subseteq G\), the \textit{centralizer} and \textit{normalizer} of \(S\) are given by
  \[C_G(S) := \{a \in G | \forall g \in S, ag = ga\}, N_G(S) := \{a \in G: aSa^{-1} = S\}\]
  respectively

  \item For \(g, h \in G\), if \(\exists a \in G, h = aga^{-1}\) then \(h\) and \(g\) are called \textit{conjugate}. The \textit{conjugacy class} of \(g\) in \(G\) is given by
  \[\ccl_G(g) := \{aga^{-1} | a \in G\}\]

  \item For subsets \(S, T \subseteq G\), if \(\exists a \in G, T = aSa^{-1}\), then \(S, T\) are called \textit{conjugate}.

\end{itemize}

\section*{Conjugation (4.3)}

\(G \acts G\) by conjugation: \(g \cdot a = gag^{-1}\). The orbits are called \underline{conjugacy classes} (as defined above), with
\[|\ccl(a)| = \frac{|G|}{|c_g(S)|}\]


\subsection*{Class Equation}
The Class Equation is given by
\[|G| = |Z(G)| + \sum_{i = 1}^r|G : C_G(g_i)|\]
where \(g_1,...,g_r\) are representations of the conjugacy classes of size greater than 1.

There's one nice application we can see right away: if \(|G| = p^n\) where \(p\) is a prime and \(n \geq 0\), \(G\) is called a ``p group'', and \(Z(G) \neq 1\).
\begin{proof}
  If \(g \notin Z(g)\), then
  \[|\ccl(g)| > 1 \land |\ccl(g)| \ | \ |G| \implies |\ccl(g)| = p^s \land s > 0\]
  is divisible by \(p\). So, by the class equation,
  \[|G| = |Z(G)| + \sum|\ccl(g_i)| \implies p \ | \ |Z(G)|\]
\end{proof}
\begin{corollary}
  If \(|G| = p^2\) (where \(p\) is prime) then \(G\) is abelian and, in fact,
  \[G \simeq \intmod{p^2} \ \lor \ G \simeq \intmod{p} \times \intmod{p}\]
\end{corollary}
Remark: if \(|G| = p^3\), \(G\) does not have to be abelian (e.g. \(D_8, Q_8\) where \(p = 2\)).
\begin{proof}
  By the above, \[Z(G) \neq 1 \implies |Z(G)| = p \text{ or } p^2\] So \(G/Z(G)\) is of order 1 or \(p\) and hence cyclic implying abelian. Hence, by Homework 4, \(G\) is abelian (in fact this implies \(Z(G) = G\))
\end{proof}

\subsection*{Conjugation in \(S_n\)}

Let \(\sigma \in S_n\), and let's see what happens when we conjugate an arbitrary \(k\)-cycle. We obtain
\[\sigma(1\ 2\ ...\ k)\sigma^{-1} = (\sigma(1)\ \sigma(2)\ ...\ \sigma(k))\]
So in the same way, we now get
\[\sigma[(1\ 2\ ...\ k)(k + 1\ ... \ l)(...)]\sigma^{-1} = (\sigma(1)\ ...\ \sigma(k))(\sigma(k + 1)\ ...\ \sigma(k))(...)\]
We see that conjugation preserves cycle types. Note that this is not necessarily true for automorphisms in general.

\begin{proposition}
  \(\tau_1, \tau_2 \in S_n\) are conjugate in \(S_n\) if and only if \(\tau_1, \tau_2\) have the same cycle type.
\end{proposition}
\begin{proof}
  \begin{itemize}

    \item[\(\implies\)] above!

    \item[\(\impliedby\)] Let
    \[\tau_1 = (i_1\ ...\ i_k)(i_1'\ ...\ i_{k'}')...\]
    \[\tau_2 = (j_1\ ...\ j_k)(j_1'\ ...\ j_{k'}')...\]
    Define \(\sigma \in S_n\) by \(\sigma(i_\ell) = j_\ell, \sigma_{i_\ell'} = j_\ell'\), etc...
  \end{itemize}
\end{proof}
Hence, the number of conjugacy classes in \(S_n\) is equal to the number of cycle types, which is equal to the number of partitions \(n = n_1 + ... + n_r\) where \(n_1 \geq n_2 \geq ...\). For example, for \(n = 4\),
\[|S_4| = 4 = 3 + 1 = 2 + 2 = 2 + 1 + 1 = 1 + 1 + 1 + 1\]
giving 5 different conjugacy classes.

\begin{theorem}
  \(\forall n \geq 5\), \(A_n\) is simple
\end{theorem}
\begin{proof}
  Suppose \(N \lhd A_n\), \(N \neq 1\). In order to show that \(A_n\) is simple, we must show \(N = A_n\).
  \begin{lemma}
    Every \(r \in A_n\) is a product of 3-cycles.
  \end{lemma}
  \begin{proof}
    We know \(\sigma\) is a product of an even number of \(2\) cycles. Observe
    \[(1\ 2)(2\ 3) = (1\ 2\ 3)\]
    \[(1\ 2)(3\ 4) = (1\ 2)(2 \ 3)(2\ 3)(3\ 4) = (1\ 2\ 3)(2\ 3\ 4)\]
  \end{proof}
  \begin{lemma}
    If \(N\) contains a 3-cycle then \(N = A_n\)
    \label{lemma:3cycle}
  \end{lemma}
  \begin{proof}
    \begin{claim}
      \(\forall \sigma \in S_n, \sigma(1\ 2\ 3)\sigma^{-1} \in N\)
    \end{claim}
    \begin{proof}
      \begin{itemize}
        \item If \(\sigma \in A_n\), true as \(N \lhd A_n\)
        \item If \(\sigma \notin A_n\), then there is an even permutation \(\tau \in A_n\) such that
        \[\sigma = (1\ 2)\tau \implies \sigma(1\ 2\ 3)\sigma^{-1} = \tau(1\ 2)(1\ 2\ 3)(1\ 2)^{-1}\tau^{-1} = \tau(2\ 1\ 3)\tau^{-1} = \tau(1\ 2\ 3)^{-1}\tau^{-1}\]
        So since \(\tau\) is even,
        \[(1\ 2\ 3) \in N \implies \tau(1\ 2\ 3)^{-1}\tau^{-1} \in N\]
      \end{itemize}
    \end{proof}
  \end{proof}
  We proceed by cases
  \begin{itemize}

    \item Assume \(N\) contains \(\sigma\) with cycle \(\geq 4\) (in its disjoint cycle decomposition). Then without loss of generality, let
    \[\sigma = (1\ 2\ 3\ ...\ r)\sigma'\]
    Let \(r = 4\) for simplicity. Since \(N\) is closed under conjugation by things in \(A_n\) and \(\sigma^{-1} \in N\),
    \[((1\ 2\ 3)\sigma(1\ 2\ 3)^{-1})\sigma^{-1} = (2\ 3\ 1\ 4)\cancel{\sigma'(\sigma')^{-1}}(4\ 3\ 2\ 1) = (1\ 2\ 4) \in N\]
    implying \(N = A_n\) by Lemma \ref{lemma:3cycle}.

    \item Assume \(N\) contains \(\sigma\) with more than 2 cycles of length 3. Then without loss of generality
    \[\sigma = (1\ 2\ 3)(4\ 5\ 6)\sigma'\]
    So
    \[(1\ 2\ 4)\sigma(1\ 2\ 4)^{-1}\sigma^{-1} = (2\ 4\ 3)(1\ 5\ 6)\cancel{\sigma'\sigma'^{-1}}(6\ 5\ 4)(3\ 2\ 1) = (1\ 2\ 5\ 3\ 4) \in N\]
    We can now apply Case 1 to get \(N = A_n\)

    \item Assume \(N\) contains \(\sigma\) with precisely one 3-cycle. Then, without loss of generality,
    \[\sigma = (1\ 2\ 3)\sigma' \implies \sigma^2 = (1\ 2\ 3)^2(\sigma')^2 =  (1\ 3\ 2) \in N \implies N = A_n\]
    by Lemma \ref{lemma:3cycle}

    \item Assume \(N\) contains \(\sigma\) which is a product of disjoint 2-cycles. We know there are \(\geq 2\) 2-cycles since \(\sigma\) is an even permutation.

    I won't write this part down on the board, since it's a little painful, but I'll post it on Quercus. It's a similar kind of argument...
  \end{itemize}
  Hence, \(A_n\) is a simple group.
\end{proof}

\subsection*{Exercises}

\begin{enumerate}

  \item

  \item

  \item \begin{itemize}
    \item What is the size of the conjugacy class of a 3-cycle in \(S_5\)?

      We know that the conjugacy class of 3-cycles is just the set of 3 cycles. Hence, since we have \({5 \choose 3}\) choices for the elements which are cycled, and 2 distinct 3-cycles in \(S_3\), giving
      \[2 \cdot {5 \choose 3} = 20\]
      different 3-cycles.

      Another way of thinking about this is that there are 5 choices for the first element, 4 choices for the second and 3 choices for the third, but each choice is counted 3 times, since cycles are invariant under cyclic permutations, of which there are 3. So we get
      \[\frac{5 \cdot 4 \cdot 3}{3} = \frac{60}{3} = 20\]

    \item What is the size of the centralizer of a 3-cycle in \(S_5\)?

    We know that \(1, \sigma, \sigma^2\) must be in the centralizer. So we have a subgroup of order 3 inside. We also have one more element: the two-cycle of the two elements not in \(\sigma\). Since the order of the centralizer is 6, and these elements have 6 products, we have that these products give the centralizer of \(\sigma\).

    \item What is the size of the conjugacy class of a \(k\)-cycle in \(S_n\)?
    \item What is the size of the centralizer of a \(k\)-cycle in \(S_n\)?
  \end{itemize}

  \item

  \item

  \item

\end{enumerate}

\end{document}
