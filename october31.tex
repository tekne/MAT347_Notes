\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 31 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Recall the following definitions from last time:
\begin{definition}
  The \underline{commutator} of \(x, y\) is defined as
  \[\cmt{x}{y} \defeq xyx^{-1}y^{-1}\]
\end{definition}
Note that the book uses the definition \(x^{-1}y^{-1}xy\), which for us is just \(\cmt{x^{-1}}{y^{-1}}\).
\begin{definition}
  The commutator subgroup \(G'\) is defined as
  \[G' \defeq \generate{\cmt{x}{y} : x, y \in G} = \{\cmt{x_1}{y_1}...\cmt{x_n}{y_n} : n \geq 0\}\]
\end{definition}
We have that \(G' \lhd G\) and \(G/G'\) is abelian. If \(N \lhd G\),
\[G/N \text{ abelian} \implies G' \leq N\]
\(G'\) is actually a characteristic subgroup of \(G\):
\[\forall \varphi \in \Aut G, \varphi(G') = G'\]
which is the same as just saying
\[\forall \varphi \in \Aut G, \varphi(G') \subset G'\]
similar to for normal subgroups, since we can always apply the above statement to the inverse \(\varphi^{-1}\) as well to get equality.

\begin{theorem}[Universal property]
  If \(\varphi: G \to A\) is a homomorphism, where \(G\) is a group and \(A\) is Abelian, then \(\varphi\) factors uniquely through \(G/G'\), i.e. there is a unique homomorphism \(G/G'\) to \(A\) such that
  \[\begin{tikzcd}
    G \arrow[rd, "\varphi"] \arrow[r, "\pi"] & G/G' \arrow[d, "\bar{\varphi}"]\\
    & A
  \end{tikzcd}\]
  commutes, i.e \(\varphi = \bar{\varphi} \circ \pi\).
\end{theorem}

We will now do some questions from the worksheet:
\begin{itemize}

  \item [4.] Assume \(N \lhd G, K \leq G, N \cap K = \{1\}, NK = G\). By (1), every element is uniquely \(nk\) (where \(n \in N, k \in K\)). So for \(n_1, n_2 \in N\), \(k_1, k_2 \in K\)
  \[(n_1k_1)(n_2k_2) = (n_1k_1n_2k_1^{-1})(k_1k_2)\]
  We have that \(k_1k_2 \in K\) and \(k_1n_2k_1^{-1} \in N\) implying \(n_1k_1n_2k_1^{-1} \in N\).

  This hence gives a way to express \((n_1k_1)(n_2k_2)\) in terms of a product \(nk\) of an element \(n \in N, k \in K\) as desired.

  Hence, we can think of a group operation \(\star\) on \(N \times K\)
  \[(n_1, k_1) \star (n_2, k_2) \defeq (n_1 \cdot k_1n_2k_1^{-1}, k_1k_2)\]
  which is isomorphic to \(G\) via the homomorphism
  \[N \times K \to G, (n, k) \mapsto nk\]
  (since each \(nk\) is unique). This is denoted
  \[G \cong N \rtimes K\]
  the \underline{internal semidirect product} of \(N\) and \(K\).

  \item [5.] Suppose now \(N, K\) are two abstract groups, i.e. not necessarily embedded as subgroups of a third group, and that we are given a homomorphism
  \[\varphi: K \to \Aut N\]
  i.e. for each element \(k\), we are given an automorphism
  \[\varphi(k): N \to N\]
  of \(N\). Inspired by our computation in (4), we can define a new group structure \((N \times K, \star)\) on the set \(N \times K\) using the following operation:
  \[(n_1, k_1) \star (n_2, k_2) \defeq (n_1 \cdot \varphi(k_1)(n_2), k_1k_2)\]
  We can invert under this operation as follows:
  \[(n, k) \star (n', k') = (1, 1) \implies k' = k^{-1} \land \varphi(k)(n') = n^{-1} \implies n = (\varphi(k))^{-1}(n^{-1}) = \varphi(k^{-1})(n^{-1})\]
  \[\implies (n, k)^{-1} = (\varphi(k^{-1})(n^{-1}), k^{-1})\]
  Check that this operation is associative.

  We call the group \((N \times K, \star)\) so defined \(N \rtimes_\varphi K\), the \underline{semidirect product of \(N\) and \(K\) with respect to \(\varphi\)}. Note that for the internal semidirect product we defined in (4), we have that
  \[\varphi: K \to \Aut N, k \mapsto (n \mapsto knk^{-1})\]
  gives
  \[N \rtimes_\varphi K = N \rtimes K\]
  That is, every internal semidirect product is an external semidirect product.

  \item [6.] We show that \(N, K\) are both isomorphic to subgroups \(\widetilde N, \widetilde K\) respectively of \(N \rtimes_\varphi K\) and that \(\widetilde N \lhd N \rtimes_\varphi K\).

  Specifically, we claim that
  \[\widetilde K = \{(1, k) : k \in K\} \cong K, \widetilde N = \{(n, 1) : n \in N\} \cong N\]
  \begin{proof}
  \begin{itemize}

    \item \(\widetilde K \cong K\): consider the homomorphism
    \[K \to N \rtimes_\varphi K, k \mapsto (1, k)\]

    \item \(\widetilde N \cong N\): consider the homomorphism
    \[N \to N \rtimes_\varphi K, n \mapsto (n, 1)\]

    \item \(\widetilde N\) is normal: we can directly check by conjugation, but it is easier to consider the homomorphism
    \[N \rtimes_\varphi K \to K, (n, k) \mapsto k\]
    This is clearly a homomorphism with kernel \(\widetilde N\), implying \(\widetilde N\) must be normal.

  \end{itemize}

  Check that \(N \rtimes_\varphi K\) is an internal semidirect product of \(\widetilde N\) and \(\widetilde K\):
  \[\widetilde N \cap \widetilde K = \{1\}\]
  \[[\forall (n, k) \in N \rtimes_\varphi K, (n, 1)(1, k) = (n\varphi(1)(1), k) = (n, k)] \implies \widetilde N \widetilde K = N \rtimes_\varphi K\]
  Moreover, the map \(\widetilde\varphi: \widetilde K \to \Aut \widetilde N\) where \(\widetilde K\) acts on \(\widetilde N\) by conjugation is equivalent to the map \(\varphi\) under the isomorphisms \(\widetilde N \cong N\) and \(\widetilde K \cong K\). That is,
  \[(1, k)(n, 1)(1, k)^{-1} = (\varphi(k)(n), 1)\]

  \end{proof}

  \item [7.] We now look at an example of a semidirect product. Specifically, we show that \(D_{2n}\) is isomorphic to a semidirect product of \(\intmod{n}\) and \(\intmod{2}\).

  We will use an internal semidirect product, with
  \[N = \langle r \rangle \cong \intmod{n}, K = \langle s \rangle = \{1, s\} \cong \intmod{2}\]
  Then
  \[N \cap K = \{1\}, NK = D_{2n}\]
  From this, we get
  \[D_{2n} = N \rtimes K \cong \intmod{n} \rtimes \intmod{2}\]

  \item [8.] Let \(F\) be a field and consider \(N = F, K = F^\times\). We can define the natural linear map
  \[\varphi: F^\times \to \Aut F, \lambda \mapsto (x \mapsto \lambda x)\]
  Then the semidirect product \(F \rtimes_\varphi F^\times\) represents affine linear maps under composition (high school ``linear functions'') from \(F\) to \(F\), i.e. \(x \mapsto mx + b\), with
  \[(b, m) = (m, 1) \star (1, b) \mapsto (x \mapsto mx + b)\]
  We can also think of this group as isomorphic to the subgroup
  \[\left\{\begin{pmatrix} a & b \\ 0 & 1 \end{pmatrix} : (a, b) \in F^2\right\} \leq \GL{2}{F}\]

  \item [Ex.] Another example (not on the worksheet) is the following: let \(|G| = pq\) with \(p < q\) primes. Recall that the \(q\)-Sylow group \(Q \lhd G\) and the \(p\)-Sylow group \(P \lhd G\).

  If \(q \not\equiv 1 \mod p\), then
  \[G \cong \intmod{p} \times \intmod{q}\]
  which is actually cyclic, since if there is only one group of any order it must be the cyclic one.

  If \(q \equiv 1 \mod p\), then we can apply (4) along with the facts that \(Q \cap P = \{1\}\) and \(QP = G\) (by Lagrange's theorem) to deduce that
  \[G = Q \rtimes P\]
  Note this is an \textit{internal} semidirect product, so we write \(=\) according to the Professor's conventions.

  Now let's try to construct a nonabelian
  \[Q \rtimes_\varphi P\]
  So we need to find an appropriate map
  \[\varphi: P \to \Aut Q\]
  which, due to isomorphism, we can assume without loss of generality to be a map
  \[\varphi: \intmod{p} \to \Aut \intmod{q}\]
  Since \(\Aut \intmod{q} \cong (\intmod{q})^\times\), we can instead find a map
  \[\varphi: \intmod{p} \to (\intmod{q})^\times\]
  \((\intmod{q})^\times\) is of order \(q - 1\), which by assumption is divisible by \(p\). By Cauchy's theorem, if \(p \divides |G|\), then \(G\) has an element of order \(p\).

  So \((\intmod{q})^\times\) has a subgroup \(H \cong \intmod{p}\). Take the isomorphism
  \[\varphi: \intmod{p} \to H \subset (\intmod{q})^\times\]
  Then
  \(\intmod{q} \rtimes_\varphi \intmod{p}\)
  is nonabelian since \(\varphi\) tells us how \(\widetilde K\) acts on \(\widetilde N\) by conjugation. Since we use \(\varphi \neq 1\),
  \[(1, k) \star (n, 1) \star (1, k)^{-1} = (\varphi(k)(n), 1) \neq (n, 1)\]
  implying the semidirect product cannot be abelian.

  There's in fact a unique, up to isomorphism, nonabelian group of order \(pq\).

\end{itemize}

\section*{Free Groups (6.3)}

Let \(S\) be a set. We want a ``free group'' defined \(\langle s \in S | \text{ no relations } \rangle\), e.g. \(S = \{x, y\}\).
So elements should look like
\[xyx^{-1}x^{-1}yy...\]
a ``word'' for the computer scientists in the audience. Of course, we still need \(xx^{-1} = 1, 1x = x\), etc. Consider
\[S = \{\}\]
Clearly, the free group here is just \(\{1\}\). On the other hand, \(S = \{x\}\) gives the free group
\[\{x^z : z \in \ints\} \cong \ints\]
\begin{definition}
  A \underline{free group generated by/on \(S\)} is a group \(F(S)\) containing \(S\) such that for any function \(h: S \to G\), where \(G\)is a group, there is a unique homomorphism
  \[\widetilde h: F(S) \to G\]
  such that \(\widetilde h_S = h\).
\end{definition}
Note that this can only be true if there are no group relations.

\end{document}
