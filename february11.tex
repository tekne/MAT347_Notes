\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{February 11 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Deg}{deg}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Ev}{ev}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Recall the definition of a field extension \(K/F\): an injective homomorphism \(F \to K\) often thought of as \(F\) being a subfield of \(K\). Note the bigger field goes first.
\begin{definition}
  \(\alpha \in K\) is \underline{algebraic} over \(F\) if \(p(\alpha) = 0\) for some nonzero \(p(x) \in F[x]\). Otherwise, \(\alpha\) is called \underline{transcendental}.
\end{definition}
Examples of transcendental numbers over \(\rationals\) are \(e, \pi\). Note that all of this depends on the base field: \(e, \pi\) are obviously algebraic over \(\reals\) (being zeroes of \(x - \pi\) and \(x - e\) respectively).
\begin{definition}
  The \underline{evaluation homomorphism of \(\alpha\)}, in this course's notation \(\Ev_\alpha: F[x] \to K\) is given by
  \begin{equation}
    p(x) \mapsto p(\alpha)
  \end{equation}
\end{definition}
Note that \(\Ev_\alpha\) is an injection if and only if \(\alpha\) is transcendental. We can also show that:
\begin{itemize}
  \item If \(\alpha\) is transcendental, \(F[x] \cong F[\alpha]\)
  \item If \(\alpha\) is algebraic, \(F[\alpha] \cong F[x]/(m(x))\)
  where \(m(x)\) is a unique irreducible monic polynomial, called the \underline{minimum polynomial of \(\alpha\) over \(F\)}.

  A consequence of this is that \((m(x))\) is a nonzero prime ideal in a PID, implying it is maximal and hence that \(F[\alpha] = F(\alpha)\).

  For example, consider \(F = \rationals\). Taking \(\alpha = \sqrt[3]{2}\), we have
  \begin{equation}
    m_{\alpha, \rationals}(x) = x^3 - 2 \implies \rationals(\sqrt[3]{2}) \cong \rationals[x]/(x^3 - 2)
  \end{equation}
  Note that it doesn't matter which cube root of 2 we choose in this case. Specifically, writing \(\alpha' = \sqrt[3]{2}\omega, \alpha'' = \sqrt[3]{2}\omega^2\) where \(\omega\) is a cube root of unity in the complex numbers, we obtain
  \begin{equation}
    \rationals(\alpha) \cong \rationals(\alpha') \cong \rationals(\alpha'')
  \end{equation}
\end{itemize}
\begin{theorem}
  If \(f(x) \in F[x]\) is a nonconstant polynomial, then there exists a field extension \(K/F\) and some \(\alpha \in K\) such that \(f(\alpha) = 0\).
\end{theorem}
This is quite surprising in a way: no matter what irreducible polynomial equation you have, there always exists a root in some field. I think in the old days when people first studied algebra this was a bit mysterious: they just took some root but didn't even talk about where that root even lives. But now, it's actually not so difficult to construct such an extension. Let's go ahead and do so:
\begin{proof}
  Pick an irreducible factor \(g(x)|f(x)\). It's enough to find a root of \(g(x)\), i.e. we reduce to the case where \(f\) is irreducible, but I'll stick to calling it \(g\). So how do we find a root? Let
  \begin{equation}
    K = F[x]/(g(x))
  \end{equation}
  which is a field since \((g(x))\) is maximal since \(g(x)\) is irreducible, as earlier today. Hence, the homomorphism from \(F\) to \(F[x]/(g(x))\) given by composing the natural inclusions from \(F\) to \(F[x]\) and from \(F[x]\) to \(F[x]/(g(x))\) is a field extension, since all homomorphisms between fields are injections. Now, let
  \begin{equation}
    \alpha = \bar{t} = \text{ the coset } t + (g(t))
  \end{equation}
  All that's left is to check it's a root: let
  \begin{equation}
    g(x) = \sum_{i = 0}^na_ix^i
  \end{equation}
  Then,
  \begin{equation}
    g(\bar{t}) = \sum_{i = 0}^na_i\bar{t}^i = \overline{\sum_{i = 0}^na_it^i} = \overline{(g(t))} = 0
  \end{equation}
\end{proof}
Now, we get to a somewhat abstract theorem which, I don't exactly know the history of that term, but I will follow Alfonso and call ``Theorem A''.
\begin{theorem}['Theorem A']
  Let \(K/F\), \(K'/F'\) be field extensions, and assume we have an isomorphism \(\varphi: F \to F'\), an irreducible polynomial \(f(x) \in F[x]\), a root \(\alpha \in K\) of \(f(x)\) and a root \(\beta \in K'\) which is a root of \(\tilde\varphi(f(x))\), where here \(\tilde\varphi: F[x] \to F'[x]\) is the natural mapping
  \begin{equation}
    \sum a_nx^n \mapsto \sum\varphi(a_n)x^n
  \end{equation}
  Then there is a unique isomorphism \(\psi: F(\alpha) \to F'(\beta)\) such that \(\psi(\alpha) = \beta\) and \(\psi|_F = \varphi\).
\end{theorem}
The naive idea is that we want an isomorphism
\begin{equation}
  \psi\left(\sum a_n\alpha^n\right) = \sum\psi(a_n)\psi(\alpha)^n = \sum\psi(a_n)\beta^n
\end{equation}
The question is: why is this well defined?
\begin{proof}
  Instead, use \(F(\alpha) \cong F[x]/(f(x))\), where \(f(x)\) is the minimal polynomial. Now, it becomes easier: we want to write down a homomorphism from \textit{this} ring to \(F'(\beta)\), and from here we can use the First Isomorphism Theorem: consider
  \begin{equation}
    \theta : F[x] \to F'(\beta), \sum a_nx^n \mapsto \sum \psi(a_n)\beta^n
  \end{equation}
  It is easy to check that this is a ring homomorphism. All we must do now is check it's surjective and compute the kernel.

  Clearly \(\theta\) is surjective, and then for the kernel, we get
  \begin{equation}
    \sum\varphi(a_n)\beta^n = 0 \implies \sum\varphi(a_n)x^n \in (m_{F', \beta}(x)) = (\tilde\varphi(f(x)))
  \end{equation}
  This is true if and only if
  \begin{equation}
    \tilde\varphi\left(\sum a_nx^n\right) \in (\tilde\varphi(f)) \iff \sum a_nx^n \in (f) \implies F(a) \cong F[x]/(f) \tilde\to F'(\beta) 
  \end{equation}
\end{proof}

\end{document}
