\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 17 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Permutations}

\(S_n\) is a nonabelian group for \(n \geq 3\), as
\[(1 2) \circ (2 3) = (1 2 3) \neq (1 3 2) = (2 3) \circ (1 2)\]
Just like matrix groups, these are natural examples of nonabelian groups.

We will now introduce another class of groups which are called dihedral groups:
\section*{Dihedral Groups (1.2)}
A dihedral group
\[D_{2n} = \text{symmetries of a regular } n \text{-gon (in the plane)}\]
This is only interesting when \(n \geq 3\).

What kind of symmetries (rigid motions of the plane) leave the \(n\)-gon invariant? We can perform
\begin{itemize}
  \item \underline{rotations} by \(\frac{2k\pi}{n}\) with \(k \in \{0,...,n - 1\}\)
  \item \underline{reflections} around an axis which passes through the center and connects either two vertices or two midpoints if \(n\) is even, or reflections around exes which pass through a vertex and a midpoint if \(n\) is odd.
\end{itemize}

In total, we get \(2n\) symmetries: \(n\) rotational symmetries and \(n\) reflections. \(D_{2n}\) is a group under composition. How can we manipulate this group? Let's begin by introducing some better notation: let \(r := \) rotation by \(\frac{2\pi}{n}\). Then rotations in \(D_{2n} : 1, r, r^2, ..., r^{n - 1}\). Let \(s := \) any reflection (\(|s| = 2\)). Claim:
\[D_{2n} = \{1, r, r^2, ..., r^{n - 1}, s, rs, r^2s, ..., r^{n - 1}s\}\]
We just need to check that no two of these are equal. We know none of the \(s, ..., r^{n - 1}s\) can be rotations, since their determinants are \(-1\). They are each distinct by the cancellation law.

Now, how can we figure out the multiplication table for for \(D_{2n}\)?
\begin{claim}
  \(sr = r^{-1}s\)
\end{claim}
\begin{proof}
  Think of \(\reals^2 = \comps\). \(r(z) = e^{2\pi i/n} \cdot z\), \(s(z) = \bar{z}\). Without loss of generality, assume the \(n\)-gon has one vertex on the real line. So
  \[sr^k(z) = e^{-2\pi ki/n}\cdot\bar{z} = r^{-k}s(z)\]
\end{proof}
This implies that \(D_{2n}\) is not abelian (since \(r^{-k} \neq r^k\) for \(k \neq n\))

For example, we can now do the multiplication
\[(r^3s)(r^2s) = sr^{-3}r^2s = sr^{-1}s = rs^2\]
Note this works regardless of \(n\). The crucial identities we need are:
\[r^n = s^2 = 1, sr^k = r^{-k}s\]
We say that \(r, s\) \underline{generate} \(D_{2n}\).

\begin{fact}
  Any relation between \(r, s\) can be obtained from \(r^n = 1, s^2 = 1, sr = r^{-1}s\)
\end{fact}
We write:
\[D_{2n} = \langle r, s | r^n = s^2 = 1, sr = r^{-1}s \rangle\]
This is called a \underline{presentation}.
It's very hard in general to determine a group from a given presentation, e.g.
\[\langle x, y, z | xyx^{-1} = y^2, yzy^{-1} = z^2, zxz^{-1} = x^2 \rangle = \{1\}\]
A group without any relations is just a set of strings.

\section*{Matrix Groups (1.4)}
If \(F\) is any field,
\[GL_n(F) := \{A \in M_n(F) : A \text{ invertible}\}\]
is the ``general linear group'' (with identity \(I_n\))
For example,
\begin{itemize}
  \item [\(n = 1\):] \(GL_n(F) = F^\times = F \setminus \{0\}\)
  \item [\(n = 2\):] nonabelian
\end{itemize}
If \(F = \mathbb{F}_p\) then \(GL_n(F)\) is a finite group. Let's try to compute \(\#GL_2(\mathbb{F}_p)\).

The first column is not equal to 0, so hence has \(p\) possible choices for its first element, and \(p\) possible choices for its second element, minus the choice where both are 0, giving \(p^2 - 1\) possible choices overall. Similarly, the second column can be anything except a multiple of the first, of which there are \(p\). So
\[\#GL_2(\mathbb{F}_p) = (p^2 - 1)(p^2 - p)\]
We can do similar things for \(GL_n\).

\section*{Quaternion group (1.5)}
Complex numbers are \(a + bi\). Hamilton was thinking about how to do this with 3 imaginary quantities, but couldn't make it work. After a long time, he realized he could do this with four: a quaternion is
\[a + bi + cj + dk\]
This gives the quaternion group
\[Q_8 := \{\pm 1, \pm i, \pm j, \pm k\}\]
with multiplication given by relations \(i^2 + j^2 + k^2 = -1\), but multiplication is no longer commutative (this is a non-abelian group) and \(ij = k\). More easy to use is the fact that all cyclic permutations work the same way: \(jk = i\), \(ki = j\), \(ij = k\), and flipping implies negation: \(ji = -k, kj = -i, ik = -j\).

This is a non abelian group of order 8, and is very different from the dihedral group of order 8 (the square). It's a little tedious to check this is associative, but it can be done.


\end{document}
