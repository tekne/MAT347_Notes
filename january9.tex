\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 9 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Recall the following definition
\begin{definition}
  A \underline{unique factorization domain} (\underline{UFD}) is a domain \(R\) such that
  \begin{itemize}

    \item every non-zero non-unit is a product of irreducible elements

    \item these products are unique up to order and associates

  \end{itemize}
\end{definition}
Today we're going to be proving that the integers are a UFD, which is probably something you've been assuming all your life, and yet still something that requires a proof. We begin with the definition of a prime element, the other method (along with irreducible elements) of generalizing the notion of a prime number to general rings:
\begin{definition}
  \(x \in R\) is \underline{prime} if \(x \neq 0\), \(x\) is not a unit and
  \begin{equation}
    \forall a, b \in R, x|ab \implies x|a \lor x|b
  \end{equation}
\end{definition}
We now proceed to do problems from the worksheet:
\begin{enumerate}

  \item Claim: every prime element is irreducible.
  \begin{proof}
    Suppose we have a factorization \(x = ab\). Then
    \begin{equation}
      x|(x = ab) \implies x|a \lor x|b
    \end{equation}
    Assume without loss of generality \(x|a\). Then \(a \sim x \implies b \sim 1\)
  \end{proof}
  Note that the converse is not true in general

  \item We begin with a few definitions \begin{definition}
    Let \(a, b \in R\). \(d\) is a GCD of \(a\) and \(b\) if it is a divisor of both \(a\) and \(b\) such that any common divisor divides \(b\)
  \end{definition}
  Suppose a GCD exists. Is there just one GCD? Not necessarily: if we have more than one GCD, \(d_1, d_2\), then we must have \(d_1|d_2\) and \(d_2|d_1\), i.e. \(d_1 \sim d_2\). So GCDs are unique \textit{up to associates}. For example, under this definition, \(3, 5\) have GCD\textbf{s} \(1\) and \(-1\).
  \begin{definition}
    If \(d\) is a GCD of \(a, b \in R\), we say that \(d\) \underline{satisfies the Bézout identity} if there exist \(x, y \in R\), \(d = xa + yb\)
  \end{definition}
  Consider now the ring of polynomials with coefficients in \(\rationals\) with no degree-one term, given by
  \begin{equation}
    S = \{a_0 + a_2X^2 + a_3X^3 + ... + a_nX^n : a_i \in \rationals\} \subset \rationals[X]
  \end{equation}
  which is a subring and hence an integral domain.
  \begin{enumerate}

    \item Do \(X^2\) and \(X^3\) have a GCD? There are no degree 1 polynomials in \(S\), so any factorization \(X^2 = f \cdot g\) must one of \(f, g\) having degree 0, since both \(f, g\) must have either degree 2 or 0, impyling \(X^2\) is irreducible, since all elements of degree 0 are units.
    Similarly, \(X^3\) is irreducible. Therefore, the common divisors are the units, of which any is a GCD.

    \item Now consider the polynomials \(X^5\) and \(X^6\). We certainly know \(X^5 = X^3 \cdot X^2\), and \(X^6 = X^3 \cdot X^3 = X^2 \cdot X^4\), so \(X^3\) and \(X^2\) are common divisors.

    By the reasoning from before, any common divisor can have degree at 3, 2 or 0. So the GCD if it exists has degree 3. But \(X^2\not|X^3\), so we cannot have a GCD.

    \item Let \(R\) be a UFD. We claim that GCDs exist for any nonzero \(a, b\).
    \begin{proof}
      We can write
      \begin{equation}
        a = p_1...p_n = u \cdot \pi_1^{e_1}...\pi_n^{e_n}
        \label{rep}
      \end{equation}
      where \(p_i\) are irreducible or units and \(\pi_i\) are irreducible, \(\pi_i \not\sim \pi_j \forall i \neq j\).
      We can enumerate all possible divisors of \(a\) as
      \begin{equation}
        v \cdot \pi_1^{f_1}...\pi_k^{f_k}
      \end{equation}
      for \(v \in \reals^\times\), \(0 \leq f_i \leq e_i\).

      Using this form of writing the element, how can we determine the GCD? All we have to do is take the minimum powers factors \(\pi\) in the representation given in equation \ref{rep}. That is, if we write
      \begin{equation}
        a = \pi_1^{e_1}...\pi_n^{e_n}, b = \pi_1^{e_1'}...\pi_n^{e_n'}
      \end{equation}
      (where factors \(\pi_i\) not common to \(a, b\) may be assigned \(e_i = 0\) in the appropriate product), then the GCD exists and is associate to
      \begin{equation}
        g = \pi_1^{\min(e_1,e_1')}...\pi_n^{\min(e_n,e_n')}
      \end{equation}
    \end{proof}
    We have the following related definition
    \begin{definition}
     \(R\) is a \underline{GCD domain} if every pair of nonzero elements have a GCD
    \end{definition}
    Remark: this implies that the ring \(S\) in (2) is not a UFD, since we showed it is not a GCD domain

  \end{enumerate}

  \item We begin by defining a Bézout domain as follows:
  \begin{definition}
    \(R\) is a \underline{Bézout domain} if every pair of nonzero elements have a GCD which satisfies the Bézout identity
  \end{definition}
  We claim that in a Bézout domain, irreducible elements are prime. The proof can be found in Alfonso's notes.

  \item We now proceed to translate statements in terms of ideals:
  \begin{enumerate}

    \item \(a\) is a unit \(\iff\) \((a) = R\)

    \item \begin{itemize}
      \item [] \(a|b\) (\(a\) divides \(b\))
      \item [\(\iff\)] if \(I\) is an ideal, \(a \in I \implies b \in I\)
      \item [\(\iff\)] \((a) \supseteq (b)\)
    \end{itemize}

    \item \begin{itemize}
      \item [] \(a \sim b\) (\(a\) and \(b\) are associates)
      \item [\(\iff\)] if \(I\) is an ideal, \(a \in I \iff b \in I\)
      \item [\(\iff\)] \((a) = (b)\)
      \end{itemize}

    \item \begin{itemize}
      \item [] \(p\) is irreducible
      \item [\(\iff\)]\((p)\) is maximal among proper principal ideals, since if \(p = ab\) is not trivial than \((p) \subset (a) \subset R\) where \(\subset\) denotes proper inclusion.
      \end{itemize}

    \item \begin{itemize}
      \item []\(p\) is prime
      \item [\(\iff\)] if \(I\) is an ideal and \(p|ab\), \(p \in I \implies a \in I \lor b \in I\)
      \item [\(\iff\)] \((p) \supseteq (ab) \implies (p) \supseteq (a) \lor (p) \supseteq (b)\)
      \end{itemize}

    \item \begin{itemize}
      \item [] \(c\) is a common divisor of \((a)\) and \((b)\)
      \item [\(\iff\)] if \(I\) is an ideal, \(c \in I \implies a, b \in I\)
      \item [\(\iff\)] \((c) \supseteq (a) + (b) = (a, b)\)
      \end{itemize}

    \item \begin{itemize}
      \item [] \(d\) is a GCD of \(a\) and \(b\)
      \item [\(\iff\)] \((d)\) is maximal among ideals of the form \((c)\), where \(c\) is a common divisor of \(a, b\)
      \end{itemize}

    \item \begin{itemize}
      \item [] There exist \(x, y \in R\) such that \(d = ax + by\)
      \item [\(\iff\)] \(d \in (a) + (b) = (a, b)\)
      \item [\(\iff\)] \((d) \subseteq (a, b)\)
      \end{itemize}

    \item \begin{itemize}
      \item [] \(R\) is a Bézout domain
      \item [\(\iff\)] for all GCDs \(d\) of \(a, b\), \((d) = (a, b)\)
      \item [\(\iff\)] for all \(a, b \in R\), \((a, b)\) is principal
      \end{itemize}

  \end{enumerate}

\end{enumerate}
Next time, we'll discuss (j) and finally prove that every PID is a UFD.

\end{document}
