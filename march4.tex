\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 4 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Deg}{deg}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Ev}{ev}
\DeclareMathOperator{\Char}{char}
\DeclareMathOperator{\Gal}{Gal}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Last time, we defined
\begin{equation}
  \Delta_2 = \{z \in \comps : \text{ constructible from } \{0, 1\} \text{ using a straightedge and compass}\}
\end{equation}
Let's now try to characterize this set:
\begin{theorem}
  \(z \in \Delta_2\) if and only if there exists a tower of fields
  \begin{equation}
    F_0 \subset F_1 \subset ... \subset F_n
  \end{equation}
  such that \(F_0 = \rationals\), \(z \in F_n\) and \(\forall j, [F_j : F_{j - 1}] = 2\)
\end{theorem}
\begin{proof}
  \begin{itemize}

    \item [\(\implies\)]: Last time (Q8)

    \item [\(\impliedby\)]: It's enough to show that \(F_n \subset \Delta_2\). We proceed by induction: assume \(F_j \subset \Delta_2\). Take \(\omega \in F_{j + 1}\). Then
    \begin{equation}
      [F_j(\omega) : F_j] \leq [F_{j + 1} : F_j] = 2
    \end{equation}
    \begin{itemize}

      \item If \([F_j(\omega) : F_j] = 1\), then \(\omega \in F_j \subset \Delta_2\)

      \item If \([F_j(\omega) : F_j] = 2\), then \(\omega\) is the root of
      \begin{equation}
        x^2 + ax + b \implies \omega = c + d\sqrt{e}
      \end{equation}
      for some \(a, b, c, d, e \in F_j\). By Q4/5, we need to show that \(e \in \Delta_2 \implies \sqrt{e} \in \Delta_2\). We can do this by an elementary construction: taking a circle with diameter \(e + 1\) and raising a perpendicular from length \(e\) along the diameter, we have that the perpendicular intersects the circle at height \(\sqrt{e}\).

    \end{itemize}
  \end{itemize}
\end{proof}
Note that, in general, if \([K : F] = 2\) and some other condition, then \(K = F(\sqrt{\alpha})\) for some \(\alpha \in K\). What could this other condition be? Well, consider the quadratic formula:
\begin{equation}
  x = \frac{-a \pm \sqrt{a^2 - 4b}}{2}
\end{equation}
Note the problem: we can't divide by 2 if \(2 = 0\). Hence, the other condition is that \(\Char F \neq 2\). A good exercise is to think about an example.

Let's now consider another very natural question in this vein: which recular \(n\)-gons are constructible? I don't know whether the Greeks were too concerned about this question, but I think that 17 or 19 year old Gauss came along and proved that you could construct the regular 17-gon, and we'll do that to. Let's start with some criteria:

Let \(\mc{P}\) be the set of \(n\) such that a regular \(n\)-gon (i.e. all sides and angles are the same) is constructible. It should be familiar that \(\{2, 3, 4, 6\} \in \mc{P}\). One related lemma from the homework is the following:
\begin{lemma}
  \begin{enumerate}[label=(\roman*)]

    \item If \(d|n\) and \(n \in \mc{P}\) then \(d \in P\)

    \item For all \(k \in \nats\), \(n \in P \iff 2^kn \in P\)

    \item If \(m, n\) are coprime, \(m, n \in \mc{P} \iff mn \in \mc{P}\)

  \end{enumerate}
\end{lemma}
Note that the regular \(n\)-gon has vertices \(e^{2\pi ik/n}\) where \(0 \leq k \leq n - 1\), with the first vertex denoted
\begin{equation}
  \zeta_n = e^{2\pi i/n}
\end{equation}
Let's start with a definition:
\begin{definition}
  An \underline{\(n^{th}\) root of unity} is a root of \(z^n - 1 = 0\). In \(\comps\), these are given by \(e^{2\pi ik/n} = \zeta^k_n\).
\end{definition}
Note that the \(n^{th}\) roots of unity form a cyclic group under multiplication of order \(n\). Note that even without noticing this group is cyclic as a special case, we know from ring theory that any finite subgroup of \(F^\times\) is cyclic, where \(F\) is any field, so this is just one manifestation of that result.

One quick remark about this is that, if \(n = p\) is prime, we saw that \(z^p - 1 = 0\) is never irreducible, as
\begin{equation}
  (z - 1)(z^{p - 1} + z^{p  -2} + ... + z + 1) = z^p - 1
\end{equation}
An application of Eisenstein's criterion, however, tells us that the right hand side
\begin{equation}
  \frac{z^p - 1}{z - 1} \in \rationals[z]
\end{equation}
\textit{is} irreducible. Last time, we saw in Question 9 that if \(\zeta_p \in \Delta_2\) then \(p - 1 = 2^k\) for some \(k\). There are only five known values of \(p\) for which this is true. It is an exercise to show that this implies that \(p = 2^{2^k} + 1\) for some \(k\). These values are known as Fermat primes, the known ones being \(3, 5, 17, 257, 65537\).

Observe also that, in general \(n \in \mc{P} \iff \zeta_n \in \Delta_2\). One direction is obvious, but the other direction isn't too difficult: if you have some random \(n\)-gon, you can translate it to the origin, and then take the ratio of two vertices to get an \(n^{th}\) root of unity.

In the last few minutes, we'll start with the real highlight of this course.
\section{Galois Theory}
Now we get to some really beautiful mathematics, that also has a very interesting story, if you don't know. I don't think I can improvise that, but definitely read up on it. He was way ahead of his time, as he related fields and groups when people had not yet defined either. It took mathematicians a few decades to fully understand his work, whereas Galois himself was not admitted to the Ecole Polytechnique. Maybe he couldn't swim?

If we have fields \(K\) and \(F\), where \(K\) extends \(F\), there will be a beautiful bijection between fields \(F \subset E \subset K\) and the subgroups of the Galois group of \(K/F\). Naturally, this will be really useful for constructing regular \(n\)-gons (building a tower extracting square roots), as well as solving by radicals (building a tower by extracting roots). Let's give a beginning definition:
\begin{definition}
  If \(K\) is a field, \(\Aut(K)\) consists of all the automorphisms of \(K\), i.e. the \underline{field} isomorphisms from \(K\) to \(K\).
\end{definition}
Just like for the automorphism group of a group, \(\Aut(K)\) is always a group under composition.
\begin{definition}
  If \(K\) is a field and \(F \subset K\) is a subfield, then the \underline{Galois group of \(K/F\)}, denoted \underline{\(\Gal(K/F)\)}, is composed of the \(F\)-automorphisms of \(K\), i.e. the automorphisms of \(K\) which fix every element of \(F\). Written mathematically,
  \begin{equation}
    \Gal(K/F) = \{\varphi \in \Aut(K) : \varphi|_F = \Id\}
  \end{equation}
\end{definition}
For example,
\begin{itemize}
  \item \(\Gal(K/K) = \{\Id\}\)
  \item \(\Gal(\comps/\reals) = \{\Id, (a + bi \mapsto a - bi)\}\)
\end{itemize}

\end{document}
