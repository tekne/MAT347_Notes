\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 11 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Deg}{deg}
\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Ev}{ev}
\DeclareMathOperator{\Char}{char}
\DeclareMathOperator{\Gal}{Gal}
\DeclareMathOperator{\Stab}{Stab}
\DeclareMathOperator{\Orb}{Orb}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Galois Theory}

If we have a field extension \(K/F\), which usually for our discussion we'll be finite, we would like to use group theory to understand all possible intermediate fields \(E\) that lie in between \(K\) and \(F\).
We found that we can construct a correspondence between subgroups of the Galois group on \(K/F\), which is the set of field automorphisms on \(K\) fixing \(F\), and intermediate fields \(E\). Last time we discussed the functions going between them, \(\hat I, \hat G\), defining them as follows:
\begin{itemize}

  \item If \(H \leq \Gal(K/F)\), we define the \underline{fixed field of \(H\)}
  \begin{equation}
    \hat I(H) = \{\alpha \in K : \forall \varphi \in H, \varphi(\alpha) = \alpha\}
  \end{equation}
  In this way we go from a subgroup to an intermediate field.

  \item If \(E\) is an intermediate field, we define
  \begin{equation}
    \hat G(E) = \Gal(K/E) \leq \Gal(K/F)
  \end{equation}
  going from intermediate fields to subgroup

\end{itemize}
What we want to clarify now is when these two functions are inverse to each other, giving us a perfect bijection between these two sides. When this is the case, they become very powerful, allowing us to at least for small examples understand \textit{all} intermediate fields in a field extension.
Let's get started with some propositions:
\begin{proposition} We have that
  \begin{enumerate}[label=(\roman*)]
    \item For all intermediate fields \(E\), \(\hat I(\hat G(E)) \supseteq E\) \label{neqfin}
    \item For all subgroups \(H \leq \Gal(K/F)\), \(\hat G(\hat I(H)) \supseteq H\) \label{eqfin}
  \end{enumerate}
\end{proposition}
We'll later show that for finite extensions part \ref{eqfin} of the proposition is always an equality, but \ref{neqfin} is still not necessarily one.
\begin{proof}
  \begin{enumerate}[label=(\roman*)]

    \item Let \(\alpha \in E\). We have by definition that
    \begin{equation}
      \forall \varphi \in \hat G(E) = \Gal(K/E), \varphi|_E = \Id \implies \varphi(\alpha) = \Id(\alpha) = \alpha \implies \alpha \in \hat I(\hat G(E))
    \end{equation}

    \item Let \(\varphi \in H\). We need to show that
    \begin{equation}
      \varphi \in \Gal(K/\hat I(H)) \iff \forall \alpha \in \hat I(H), \varphi(\alpha) = \alpha \iff \forall \varphi' \in H, \varphi'(\alpha) = \alpha
    \end{equation}

  \end{enumerate}
\end{proof}
We'll see that if \(K/F\) is finite, \(\hat I, \hat G\) are inverse bijections provided the Galois group is ``large enough.'' The precise condition is that the Galois group needs to be of the same size as the degree of the extension \([K : F]\), which is something that we'll prove soon.

One thing I forgot to recall is that not only to extension fields have a natural order by inclusion, but \(\hat G\) and \(\hat I\) reverse this order by inclusion. If we start with \(F\) on one side and apply \(\hat G\), we go to \(\Gal(K/F)\). If we go back along \(\hat I\), we go back to the fixed field of the full Galois group, which definitely includes \(F\). But if the Galois group is not big enough, the fixed field will be \textit{too} big, i.e. will not be \(F\).

One special case is that, if \(K = F(\alpha)\), we know that
\begin{equation}
  |\Gal(K/F)| = |\{k \in K : m_{\alpha,F}(k) = 0\}|
\end{equation}
There are two ways in which we can have \(|\Gal(K/F)| < \deg m_{\alpha, F}\).
\begin{enumerate}

  \item ``normal'': \(m_{\alpha, F}(x)\) does not split into linear factors in \(K[x]\) (e.g. \(\rationals(\sqrt{2})/\rationals\)).

  \item ``separable'': \(m_{\alpha, F}(x)\) could have repeated roots. You may never have see an example of this yet, but it can happen.

\end{enumerate}

\section{Splitting Fields and Normal Extensions}

\begin{definition}
  \(f(x) \in F[x]\) \underline{splits over \(K\)} if \(f(x)\) is a product of linear factors in \(K[x]\).
\end{definition}
Now we get to the important notion of a \textit{splitting field}
\begin{lemma}
  Suppose \(f(x) \in F[x]\) splits over \(K\):
  \begin{equation}
    \exists \alpha_1,...,\alpha_n \in K, c \in F^\times, f(x) = c\prod_{i = 1}^n(x - \alpha_i)
  \end{equation}
  Then \(K = F(\alpha_1,...,\alpha_n)\) if and only if \(K\) is the smallest intermediate field in \(K/F\) over which \(f(x)\) splits.
\end{lemma}
\begin{proof}
  \(f(x)\) splits over the intermediate field \(E\) if and only if \(\alpha_1,...,\alpha_n \in E\), which is true if and only if \(E \supseteq F(\alpha_1,...,\alpha_n)\).
\end{proof}
\begin{definition}
  If these conditions hold, we say \(K/F\) is a \underline{splitting field of \(f(x)\) over \(F\)}
\end{definition}
Let's do an example: the splitting field of \(x^2 - 3\) over \(\rationals\) is \(\rationals(\sqrt{3}) = \rationals(\sqrt{3}, -\sqrt{3})\). Over \(\reals\), however, the splitting field is just \(\reals\) itself. On the other hand, the splitting field of \(x^3 - 2\) over \(\rationals\) is \(\rationals\) adjoined the three roots of this polynomial in \(\comps\), i.e.
\begin{equation}
  \rationals(\sqrt[3]{2}, \zeta_3\sqrt[3]{2}, \zeta_3^2\sqrt[3]{2}) = \rationals(\sqrt[3]{2}, \zeta_3)
\end{equation}
The degree of \(\rationals(\sqrt[3]{2})\) is 3. Using the tower law, consider the tower \(\rationals(\sqrt[3]{2}, \zeta_3) \supset \rationals(\sqrt[3]{2}) \supset \rationals\). We have that \(\zeta_3\) satisfies the polynomial of degree two \(x^2 + x + 1 = 0\) in \(\rationals(\sqrt[3]{2})\), so
\([\rationals(\sqrt[3]{2}, \zeta_3) : \rationals(\sqrt[3]{2})] \leq 2\)
On the other hand, it cannot be 1 since the two fields would be equal, and only one contains complex numbers. So it follows that
\begin{equation}
  [\rationals(\sqrt[3]{2}, \zeta_3) : \rationals(\sqrt[3]{2})] = 2
  \implies [[\rationals(\sqrt[3]{2}, \zeta_3) : \rationals] = 2 \cdot 3 = 6
\end{equation}
Next time, what we're going to show is that splitting fields always exist, that any two splitting fields are isomorphic and that they are of degree at most \(n!\).

\end{document}
