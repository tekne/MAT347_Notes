\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 12 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Groups}

\begin{definition}
  A group is a set \(G\) along with a binary operation \(*\) which is
  \begin{enumerate}
    \item Associative
    \item Has an identity \(e \in G\)
    \item \(\forall x \in G, \exists x^{-1} \in G, x \cdot x^{-1} = x^{-1} \cdot x = e\)
  \end{enumerate}
\end{definition}

Some properties:
\begin{proposition}
\begin{enumerate}
  \item The identity \(e\) is unique
  \item Inverses \(x^{-1}\) are unique for each \(x\)
  \item \((x * y)^{-1} = y^{-1} * x^{-1}\)
  \item \(x_1 * ... * x_n\) does not depend on the order of bracketing
  \item \underline{Cancellation laws:}
  \[x * y = x * z \implies y = z\]
  \[y * x = z * x \implies y = z\]
\end{enumerate}
\end{proposition}
\begin{proof} Exercise/book. As an example, we do (2):
\begin{itemize}
  \item [2.] Suppose \(x \in G\) has two inverses, \(x'\) and \(x''\).
  \[x' = x' + e = x' * (x * x'') = (x' * x) * x'' = e * x'' = x''\]
\end{itemize}
\end{proof}

\section*{Multiplication Tables}

If \(G\) is a finite group, we can make a multiplication table just like in elementary school, by writing out a table of all the possible products of two elements. Let
\[G = \{g_1 = e,...,g_n\}\]
We can write out a table
\[\begin{array}{c|cccc}
& e = g_1 & g_2 ... & g_n \\ \hline
e = g1  & g_1 & g_2 \\
\vdots  \\
g_n     & g_n & g_n * g_2\\
\end{array}\]
For example, assume \(G = \mathbb{F}_5^{\times}\). We can then write out
\[\begin{array}{c|cccc}
& \bar{1} & \bar{2} & \bar{3} & \bar{4} \\\hline
\bar{1} & \bar{1} & \bar{2} & \bar{3} & \bar{4} \\
\bar{2} & \bar{2} & \bar{4} & \bar{1} & \bar{3} \\
\bar{3} & ... \\
\bar{4} & ...
\end{array}\]

Note that every row here is a permutation of the first row. This is the first hint that finite groups have something to do with permutations. Later, we will show that every finite group is isomorphic to, ``the same as,'' a permutation group.

One proof we can do is to show that there is only one group \(G = \{e, x\}\) with two elements up to isomorphism. This is because, writing out the table for \(y = x * x\), we have
\[\begin{array}{c|cc}
& e & x \\
e & e & x \\
x & x & y \\
\end{array}\]
Since \(x\) must have an inverse, we must have \(y = e\).

So far, we've been really precise, always writing stars, but we won't always be so precise. We will now often just write \(xy\), but if we have a commutative (Abelian) group, like the integers under addition, sometimes it is more convenient to write \(x + y\) to avoid confusion with multiplication. Of course, we won't do this in say, the case of \(\reals^+\) under multiplication. For general groups, we often write \(1\) instead of the identity and \(x^{-1}\) for inverses, but if we use this additive notation, then we will use \(0\) and \(-x\). Of course, if things get confusing, it's better to use a star than to go wrong.

\begin{definition}
  Let \(x \in G, n \in \ints\). Define
  \[x^n = \left\{\begin{array}{ccc}
  x ... x \text{ } n \text{ times} & \text{if} & n > 0 \\
  e = 1 & \text{if} & n = 0 \\
  (x^{-1})...(x^{-1}) \text{ } -n \text{ times} & \text{if} & n < 0
  \end{array}\right.\]
\end{definition}

Remark: if \((G, +)\) is Abelian, we write \(x^n\) as defined above as \(nx\).

\begin{proposition}
  \(x^n \cdot x^m = x^{n + m}, (x^n)^m = x^{n \cdot m}\)
\end{proposition}
\begin{proof} Obvious if \(n, m \geq 0\). In general distinguish cases.
\end{proof}

Be careful: in general, \((xy)^n \neq x^ny^n\). This is OK if \(G\) is Abelian, however.

Exercise: if \(\forall x, y \in G, (xy)^n = x^ny^n\), show \(G\) is Abelian.

\vspace{5mm}

The order of \(x \in G\) is the smallest \(n \geq 1\) such that \(x^n = e\). Now, if we take the integers under addition, what happens when we take \(x = 1\), i.e. what is the smallest nonzero value \(n\) such that \(nx = 0\)? There is none! So it might be infinite (\(\infty\)). In the book we write \(|x|\), but this might be confusing for integers, since, for example, here \(|1| = \infty\).

\vspace{5mm}

Example: \(e\) is the only element of order \(1\), \(x\) in the group \(\{e, x\}\) and \(-1\) in the multiplicative group of \(K \setminus \{0\}\) where \(K\) is any field except that with two elements.

\section*{The group \(\ints\setminus n\ints\) (0.3)}
In MAT240, we saywhat when \(n = p\) is prime, \(\ints\setminus p\ints\) is the field \(\mathbb{F}_p\). But normally, \(\ints\setminus n\ints\) is just a group.

Fix \(n \geq 1\). Say \(a \equiv b\) if \(n | (a - b)\). Check (exercise) that this is an equivalence relation. By definition,
\[\ints\setminus n\ints := \{\text{equivalence classes in } \ints \text{ under } \equiv\}\]
Sometimes, \(\mathbb{F}_p\) is just defined as the symbols \(0,...,p-1\) with some operations, but in general it'squite important to think of them as equivalence classes.

Write \(\bar{a}\) where \(a\) is an integer for the equivalence class of \(a\). For example,
\[\bar{0} = \{nk : k \in \ints\} = \{..., -2n, -n, 0, n, 2n, ...\}\]

Each equivalence class can be represented by one of \(\bar{0},...,\overline{n - 1}\) since these are all the possible remainders when dividng.

We can now define two binary operations \(\bar{a} + \bar{b}\) and \(\bar{a} \cdot \bar{b}\), where
\[\bar{a} + \bar{b} := \overline{a + b}\]
I need to be a little bit careful, because if you haven't seen this before it's not clear that this is a well defined operation. We know there are infinitely different many symbols for each equivalence class, e.g.
\[\bar{0} = \bar{n} = \bar{2n} = ...\]
We need to check, then, that if \(\bar{a} = \bar{a'}\) and \(\bar{b} = \bar{b'}\) then
\[\overline{a + b} = \overline{a' + b'}\]
I would really like you to be comfortable with this step. It should be clear to you that there is something that you need to check, and that the definition on its own is not well defined, since later on we're going to do the same thing but in a more complicated setting where things aren't commutative. Once you have arived at this, however, it's not very hard to verify.

Once we have defined this operation, it's easy to check that \((\ints \setminus n\ints, +)\) is a (finite and Abelian) group, with \(e = \bar{0}\) and \(-\bar{n} = \overline{-n}\).

For example, in \(\ints \setminus 6\ints\), \(\bar{5} + \bar{4} = \bar{9} = \bar{3} = \bar{-1} + \bar{4}\).

\vspace{5mm}

Similarly, we define
\[\bar{a} \cdot \bar{b} = \overline{ab}\]
Note the same problems with well-definition apply. It's slightly more work in this case, but it follows exactly the same steps as above. Again, a good exercise.

Let
\[(\ints\setminus n\ints)^\times = \{\bar{a} \in \ints\setminus n\ints : \exists \bar{b}, \bar{a} \cdot \bar{b} = 1\}\]
In this way, \((\ints \setminus n\ints)^\times, \times)\) is an (abelian) group. We know the identity is \(1\), and then inverses exist because we for them to exist.

For example,
\[(\ints\setminus 9\ints)^\times = \{\bar{1},\bar{2},\bar{4},\bar{5},\bar{7},\bar{8}\}\]
We can also write
\[(\ints\setminus n\ints)^\times = \{\bar{a} \in \ints\setminus n\ints : \gcd(a, n) = 1\}\]
\begin{proof}Exercise.\end{proof}

The fact that we use equivalence classes is once again very convenient. For example, in \((\ints\setminus 60\ints)^\times\),
\[\overline{53} \times \overline{46} = \overline{-7} \times \overline{-14} = \overline{98} = \overline{38}\]

What I want to do now is introduce the next important class of groups: symmetric groups.

\section*{Symmetric Groups (1.3)}

We introduced the notation \(S_n\) to denote the set of all permutations of the set of integers from \(1\) to \(n\). \(S_n\) is a group under \(\circ\). More generally, \((S_X, \circ)\) where \(X\) is any set is a group, with \(S_X\) denoting the set of bijections from \(X\) to itself.

\subsection*{Cycle Notation}

Let's say we have some permutation of the numbers \(1,...,7\). We can depict it by showing what it does to each number, and we note that it forms ``cycles'', e.g. 1 goes to 3 goes to 4 goes to 7. We could write this cycle out as \((1 \ 3 \ 4 \ 7) = (3 \ 4 \ 7 \ 1) = ...\).

This is called a \underline{4-cycle}. Similarly, we could have a \underline{2-cycle} \((2 \ 5)\). We omit ``1-cycles'', i.e. \(n\) maps to itself, in our notation, and can then write out a compound permutation as
\[(1 \ 3 \ 4 \ 7)(2 \ 5)\]
How do we know that we are working in \(S_7\)? This is given to us beforehand, but we the same notation could easily apply to, say \(S_{10}\). On the other hand, \(S_7\) is isomorphic to a subgroup of \(S_{10}\), so it doesn't really matter.

This is called the Disjoint Cycle Decomposition, and we will later show that it is unique up to the ordering of the cycles.

We can list out symmetric groups in terms of these cycles. For example,
\[S_3 = \{e = 1, (1\ 3\ 2), (1\ 2\ 3), (1\ 2), (1\ 3), (2\ 3)\}\]
We can perform composition as by just repeatedly substituting in elements:
\[(1\ 2\ 3)(4\ 5) \circ (1\ 4)(2\ 3) = (1\ 5\ 4\ 2)\]
Remark: disjoint cycles commute, but general cycles do not commute with each other. This can ease computation. For example, the above could have been done as
\[(1\ 2\ 3)(4\ 5) \circ (1\ 4)(2\ 3)
  = (1\ 2\ 3) \circ (1\ 5\ 4) \circ (2\ 3)
  = (1\ 2\ 3) \circ (2\ 3) \circ (1\ 5\ 4)
  = (2\ 1) \circ (1\ 5\ 4)
  = (1\ 5\ 4\ 2)\]
Note, for example, we can use commutativity to show that
\[((1\ 2)(3\ 4\ 5))^n = (1\ 2)^n(3\ 4\ 5)^n = 1\]

\end{document}
