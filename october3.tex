\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 3 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}

\DeclareMathOperator{\Ima}{Im}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Quotient Groups}

Idea: imitate \(\ints \rightsquigarrow \intmod{n}\) for general \(G\) by finidng equivalence relations \(\sim\) on \(G\) such that
\[G\setminus\sim \\ = \text{ equivalence classes}\]
becomes a group.

Let \(S \subseteq G\), and define
\[a \sim b \iff \exists x \in S, b = ax \iff ba^{-1} = x\]
i.e.
\[a \sim b \iff ba^{-1} \in S\]

\begin{claim}
  Two equivalence classes are either equal or disjoint. For a set \(X\), each equivalence class \(X\) is called a ``\underline{partition of \(X\)}.''
  \label{partition}
\end{claim}
\begin{proof}
  Let \(a, b, c \in X\) be arbitrary.
  \begin{itemize}

    \item Assume \(a \sim b\). Then \(a \sim c \iff b \sim c\) implying that \(c\) is in the equivalence class of \(a\) if and only if \(c\) is in the equivalence class of \(b\), implying they are equal.

    \item Assume \(a \not\sim b\). Assume \(a \sim c\). If \(c \sim b\), then \(a \sim b\), contradicting our assumption. Similarly, assume \(b \sim c\). If \(a \sim c\), then \(a \sim b\), again contradicting our assumption. Hence \(c\) cannot simultaneously be in the equivalence classes of \(a\) and \(b\), implying they are disjoint.

  \end{itemize}
\end{proof}


\begin{enumerate}

  \item Find necessary and sufficient conditions for \(\sim\) to be an equivalence relation.

  Claim: \(\sim\) is an equivalence relation if and only if \(S \leq G\).
  \begin{proof}
    Let \(a, b, c \in G\) be arbitrary.
    \begin{itemize}
      \item We have
      \[a \sim a\]
      if and only if
      \[\exists d \in S, ad = a \implies d = 1\]
      i.e. \(1 \in S\).
      \item We have
      \[a \sim b \iff b \sim a\]
      if and only if
      \[ba^{-1} \in S \iff ab^{-1} \in S\]
      Substituting in \(b = 1\) (using the fact that \(1 \in S\)), we have
      \[a^{-1} \in S \iff a \in S\]
      \item We have
      \[a \sim b \land b \sim c \implies a \sim c\]
      if and only if
      \[ba^{-1} \in S \land cb^{-1} \in S \implies ca^{-1} \in S\]
      Choosing \(b = 1\), we have
      \[a^{-1} \in S \land c \in S \implies ca^{-1} \in S\]
      Hence, by (2),
      \[a \in S \land c \in S \iff a^{-1} \in S \land c^{-1} \in S \implies c^{-1}a^{-1} \in S \iff ac \in S\]
    \end{itemize}
    These are the group axioms, implying that \(\sim\) is an equivalence relation if and only if \(S\) is a group.
  \end{proof}
  From now on, take \(S = H \leq G\).

  \begin{notation}
    \(aH\) denotes the equivalence class of \(H\).
  \end{notation}

  \item \(aH = bH \iff a^{-1}b \in H\).
  \begin{proof}
  \(aH = bH \iff a \sim b\)
  which is true if and only if
  \[\exists x \in H, b = ax \iff a^{-1}b = x\]
  i.e. \(a^{-1}b \in H\)
  \end{proof}
  Hence, we can write
  \[aH = \{b \in G : b \sim a\} = \{ax : x \in H\}\]
  This is called the \underline{left coset}.

  Remark: there are also right cosets
  \[Ha = \text{ equivalence classes for } a \approx b \iff \exists x \in H, b = xa \iff ba^{-1} \in H\]

  \item \begin{notation}
    \(|G : H| = \#\) of left cosets.
  \end{notation}
  What is the cardinality of each coset \(aH\)? What is the relation between \(|G|\), \(|H|\) and \(|G : H|\)?
  \begin{enumerate}

    \item Cosets form a partition of \(G\) by Claim \ref{partition}.

    \item All cosets have the same size, since we can construct a bijection
    \[H \to aH, x \mapsto ax\]
    This implies
    \[|G| = |H||G : H|\]
  \end{enumerate}
  This can be stated as a theorem:
  \begin{theorem}[Lagrange's Theorem]
    \[H \leq G \implies |H| \text{ divides } |G|\]
  \end{theorem}

  \item Consider the group \(G = D_8\) and consider the two subgroups
  \[H_1 := \langle s \rangle, H_2 := \langle r \rangle\]
  \(H_1\) has 4 cosets:
  \begin{enumerate}
    \item \(H_1 = \{1, s\} = sH_1\)
    \item \(rH_1 = \{r, rs\} = rsH_1\)
    \item \(r^2H_1 = \{r^2, r^2s\} = r^2sH_1\)
    \item \(r^3H_1 = \{r^3, r^3s\} = r^3sH_1\)
  \end{enumerate}
  Note that \(s\) is order 2. On the other hand, \(r\) is order 4 and \(H_2\), correspondingly, has 2 cosets:
  \begin{enumerate}
    \item \(H_2 = \{1, r, r^2, r^3\} = rH_2 = ...\)
    \item \(sH_2 = \{s, sr, sr^2, sr^3\} = rsH_2 = ...\)
  \end{enumerate}


  \item
  \begin{notation}
    \(G / H = \{\text{left cosets of } H\}\)
  \end{notation}

  We want to try to use the operation on \(G\) to define an operation \(\star\) on the set \(G \setminus H\). Given \(aH, bH \in G / H\), we can try to define
  \[(aH) \star (bH) = (ab)H\]
  Note that \(\star\) is used to emphasize the definition of a new operation. Once we make sure this operation works and has no ambiguity, we will drop the \(\star\).

  In general \(\star\) is not well-defined. Show that with one of the subgroups in (4), the operation is well-defined, but not for the other.
  \begin{itemize}

    \item For \(H_1\),
    \[H_1 \star (rH_1) = (1r)H_1 = rH_1 = (sH_1) \star (rH_1) = (sr)H_1\]
    But \(srH_1 = \{sr, srs\} = \{r^3s, r^3\} = r^3H_1 \neq rH_1\) giving a contradiction. So \(\star\) is not defined for \(H_1\).

    \item For \(H_2\), (lots of cases to check, but it's well defined)

  \end{itemize}
  \begin{definition}
    We say that the subgroup \(H\) is a \underline{normal subgroup of \(G\)} if the operation \(\star\) in \(G/H\) is well-defined. We write \(H \lhd G\) (the book writes \(H \unlhd G\)).
  \end{definition}

  \item Assume \(H \lhd G\). In this case we know the operation in \(G / H\)is well-defined. What other conditions are required for \(G/H\) is a group with this operation.

  We check each axiom:
  \begin{itemize}

    \item Writing \(H = eH\), we already have
    \[H \star aH = H \star aH = H\]

    \item Inverses:
    \[aH \star a^{-1}H = (aa^{-1})H = H\]

    \item Associativity is inherited from the group operation.

  \end{itemize}
  Hence, no additional conditions are necessary.

  \item \begin{notation}
    Let \(G\) be a group. Given \(A, B \subseteq G\), \(x, y \in G\), we define
    \[xA := \{xa | a \in A\}\]
    \[Ax := \{ax | a \in A\}\]
    \[AB := \{ab | a \in A, b \in B\}\]
  \end{notation}
  Let \(G\) be a group and \(H \subseteq G\). Consider the following statements:
  \begin{enumerate}
    \item \(H \lhd G\)
    \item \(\forall a \in G, aH = Ha\) (note this does \textit{not} necessarily mean \(a\) commutes with the elements in \(H\), just that the sets are the same)
    \item \(\forall a \in G, aHa^{-1} = H\)
    \item \(\forall a \in G, aHa^{-1} \subseteq H\)
    \item There exists some group \(L\) and some homomorphism \(f: G \to L\) such that \(H = \ker f\).
  \end{enumerate}
  \begin{claim}
    They are all equivalent
  \end{claim}
  \begin{proof}
    \begin{itemize}
      \item \((e) \implies (b)\): Nice exercise.
      \item \((b) \implies (c)\):
      \[aHa^{-1} = a(Ha^{-1}) = a(a^{-1}H) = (aa^{-1})H = eH = H\]
      \item \((c) \implies (d)\): Trivial.
      \item \((d) \implies (a)\):
        Assume \(\forall a \in G, aHa^{-1} \subset H\). To check that \(aH \star bH = (ab)H\) is well defined, suppose we have \(aH = a'H, bH = b'H\). We need to check that \((ab)H = (a'b')H\).

        We have that
        \[aH = a'H \iff a = a'h_1, bH = b'H \iff b = b'h_2\]
        for \(h_1, h_2 \in H\). Hence,
        \[abH = a'h_1b'h_2H = a'b'H \iff (a'b')^{-1}a'h_1b'h_2 = b'^{-1}h_1b'h_2 \in H\]
        By our assumption, \(b^{-1}h_1b' \in H\), implying the above since \(H\) is a subgroup.
      \item \((a) \implies (e)\):
      \(\varphi: G \to G/H\), \(a \mapsto aH\) is a homomorphism since
      \(aH \star bH = (ab)H\).
      \begin{claim}
        \(H = \ker\varphi\)
      \end{claim}
      \begin{proof}
        \[\ker\varphi = \{a \in G : aH = H\} \iff a \in H\]
      \end{proof}
    \end{itemize}
  \end{proof}
  This becomes a useful theorem about normal subgroups.

  Condition (b) means that the left coset of \(a\) is equal to the right coset of \(a\) for all \(a \in G\), i.e. left cosets give the same partition of \(G\) as right cosets, i.e. any left coset is a right coset and vice versa.

  Examples of normal subgroups:
  We can use this to go back and check that \(H_2\) is a normal subgroup of \(D_8\) as follows:
 \begin{proof}
   We have 2 right cosets, and 2 left cosets. The left cosets are \(H_2, sH_2 = G \setminus H_2\), and the two right cosets are \(H_2, H_2s = G \setminus H_2\). So that means any left coset is a right coset, and therefore \(H_2\) is a normal subgroup.
 \end{proof}
 Conclusion: if \(H \leq G\) is a subgroup and \(|G : H| = 2\) then \(H \lhd G\).

  \item TODO

\end{enumerate}

\end{document}
