\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 24 2018}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}

\DeclareMathOperator{\Ima}{Im}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Group Actions}

First of all, what does it mean for a group \(G\) to act on a set \(A\) (\(G \acts A\))? One way to say it is that there's a function \(G \times A \to A\), \((g, a) \mapsto g \cdot a\) or \(ga\) which respects the group structuee, following the axioms
\begin{itemize}
  \item \(1 \cdot a = a\)
  \item \(g \cdot (h \cdot a) = (g \cdot h) \cdot a\)
\end{itemize}
For example,
\begin{itemize}
  \item \(S_n \acts \{1, 2, ..., n\}\) via permutation
  \item \(S_n \acts \mc{P}(\{1, 2, ..., n\})\) or all subsets of size \(k \in \{0,...,n\}\)
  \item \(S_n \acts \) ordered subsets of size \(k\) (either all distinct or allowing repetitions)
  \item \(S_n \acts \) polynomials in \(n\) variables \(x_1,...,x_n\) (permuting variables)
\end{itemize}
This is to give you the intuition that groups can act on many different sets.

Another way to think of group actions is via the permutation representation: if \(G \acts A\), then we get a homomorphism \(G \to S_A\), \(g \mapsto (a \mapsto g \cdot a)\).

\begin{proposition}
  This gives us a bijection
  \[\{\text{actions of } G \text{ on } A\} \longleftrightarrow \{G \xrightarrow[]{\text{homo}} S_A\}\]
  \[\text{action } \mapsto \text{ associated permutation representation}\]
\end{proposition}
\begin{proof}
  Inverse function: given a homomorphism \(\theta: G \to S_A\) define
  \[g \cdot a := \theta(g)(a) \in A\]
  Check: action axioms + inverse functions.
  \begin{itemize}
    \item \(g \cdot (h \cdot a) = \theta(g)(h \cdot a) = \theta(g)(\theta(h)(a)) = (\theta(g) \circ \theta(h))(a) = \theta(gh)(a) = gh \cdot a\)
  \end{itemize}
\end{proof}
Example: \(D_6\) acts on vertices of triangles, with the permutation representation being a homomorphism \(D_6 \to S_3\), with
\[r = \text{ rotation of } \frac{2\pi}{3} \mapsto (1\ 2\ 3)\]

\section*{Subgroups (2.1)}

Subgroups are not a surprising concept: they're just like subspaces in linear algebra.
\begin{definition}
  Let \((G, *)\) be a group. A subset \(H \subset G\) is a \underline{subgroup} if \((H, *)\) is a group. Write \(H \leq G\).
\end{definition}
Examples:
\begin{itemize}
  \item \(\{1\} \leq G, G \leq G\)
  \item \(K \leq H \leq G \implies K \leq G\)
  \item \(\{\text{rotations}\} \leq D_{2n}\)
  \item \(2\ints \leq \ints \leq \rationals \leq \reals \leq \comps\)
  \item \(S_n \leq S_{n + 1}\) (with elements of \(S_n\) thought of those permutations that fix the \(n + 1\)th element)
\end{itemize}
\begin{claim}
  If \(H \leq G\), then \(e \in H\) and \(h \in H \implies h^{1} \in H\)
\end{claim}
\begin{proof}
    \(H\) has an identity \(e_H\), which must be equal to \(e \in G\) by cancellation:
    \[e_H * e_H = e_H \implies e_H = e \implies e \in H\]
    If \(h \in H\), then the inverses in \(H\) and \(G\) are the same by uniqueness of inverses inside \(G\), implying \(h^{-1} \in H\).
\end{proof}
\begin{proposition}
  The following are equivalent:
  \begin{enumerate}
    \item \(H \leq G\)
    \item \(H \neq \varnothing, H\) is closed under products and inverses
    \item \(H \neq \varnothing\), \(h_1, h_2 \in H\implies h_1h_2^{-1} \in H\)
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item \((1) \implies (2)\): \(e \in H \implies H \neq \varnothing\). It is closed under products by definition, and it is closed under inverses by the Claim
    \item \((2) \implies (3)\): \(h_1, h_2 \in H \implies h_2^{-1} \in H \implies h_1h_2^{-1} \in H\)
    \item \((3) \implies (2)\): exercise
  \end{itemize}
\end{proof}
Note that \((2)\) is how the book defines groups, but \((3)\) is the most useful in practice.
\begin{proposition}
  If \(\varphi: G \to H\) is a homomorphism, then
  \begin{itemize}
    \item \(\ker\varphi := \{g \in G : \varphi g = 1\} \leq G\)
    \item \(\Ima\varphi := \{\phi(g) : g \in G\} \leq H\)
  \end{itemize}
\end{proposition}
Example: \(\varphi: \ints \to \ints\), \(x \mapsto 2x\) is a homomorphism, implying that
\[\text{im}\varphi = 2\ints \leq \ints\]
\begin{proof}
  \begin{enumerate}
    \item Homework
    \item Use subgroup criterion (3):
    \[\varphi(1) = 1 \implies 1 \in \Ima\varphi \implies \Ima\varphi \neq \varnothing\]
    If \(h_1, h_2 \in \Ima\varphi\) then
    \[\exists g_1, g_2 \in G, h_1 = \varphi(g_1), h_2 = \varphi(g_2)\]
    \[h_1h_2^{-1} = \varphi(g_1)\varphi(g_2)^{-1} = \varphi(g_1)\varphi(g_2^{-1}) = \varphi(g_1g_2^{-1})\]
  \end{enumerate}
\end{proof}
Remark: every \(H \leq G\) is the image of some homomorphism, the inclusion homomorphism
\[H \to G, h \mapsto h\]
The \underline{centre} of \(G\) is
\[Z(G) := \{g \in G : gx = xg \forall x \in G\}\]
Check \(Z(G) \leq G\):
\begin{itemize}
  \item \(e \in Z(G)\)
  \item If \(g_1, g_2 \in Z(G)\), \(g_1g_2x = g_1xg_2 = xg_1g_2 \implies g_1g_2 \in Z(G)\), and \(gx = xg \implies g^{-1}x = xg^{-1}\).
\end{itemize}
Exercise: \(Z(Q_8) = \{\pm 1\}\)

\end{document}
