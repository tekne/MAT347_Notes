\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT347 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 21 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\DeclareMathOperator{\Ima}{Im}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Inn}{Inn}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\Deg}{deg}

\def\acts{\curvearrowright}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

Last time, we discussed the Gaussian integers
\begin{equation}
  \ints[i] = \{a + bi : a, b \in \ints\}
\end{equation}
which we found to be Euclidean, and hence a PID, and hence a UFD. Note that these implications do not hold the other way around: the book contains an example of a non-Euclidean PID, and by the end of this week we will know a UFD which is not a PID.

We found that every irreducible/prime divides a prime number \(p\), i.e.
\begin{equation}
  2 = i(1 - i)^2
\end{equation}
where \(1 - i\) is irreducible and \(i\) is a unit. Specificially, we showed that a prime number \(p\) is irreducible if it is equal to \(3\) mod \(4\), or otherwise can be written as a product \(\pi \cdot \bar{\pi}\) of irreducibles, \(\pi \not\sim \bar{\pi}\). To prove this, we used the norm
\begin{equation}
  N : \ints[i] \to \nats, N(x + iy) = (x + iy)(x - iy) = x^2 + y^2
\end{equation}
Note here \(\nats = \ints_{\geq 0}\). This example is slightly misleading as it looks like every ring which is obtained in this way, that is, using the solution to some quadratic equation, gives an arithmetic which is quite similar to that of the usual integers. But that's not true, and there's something very interesting going on. For example, consider the ring
\begin{equation}
  R = \ints[\sqrt{-5}] = \{a + b\sqrt{-5} : a, b \in \ints\}
\end{equation}
We can again use the same norm
\begin{equation}
  N : Z[\sqrt{-5}] \to \nats, N(x + y\sqrt{-5}) = (x + y\sqrt{-5})(x - y\sqrt{-5}) = x^2 + 5y^2
\end{equation}
Clearly, \(N\) is multiplicative as complex conjugation is compatible with multiplication. Now, however, things are different: even if we start with some relatively small number, like \(6\), we can write it as \(2 \cdot 3\), but also as \((1 + \sqrt{-5})(1 - \sqrt{-5})\). I claim that these are not equivalent factorizations, and hence that we are not in a UFD. To do this, we need to check that they are not associates of each other. To do so, we need to find the units \(R^\times\). But the only numbers whose norm is \(1\), and hence the only units, are \(\pm 1\). We can now proceed as follows:
\begin{claim}
  \(2, 3, 1 \pm 5\) are irreducible
\end{claim}
\begin{proof}
  If not, we have a \underline{non-trivial factorization} \(2 = \alpha\beta\), implying
  \begin{equation}
    N(2) = 4 = N(\alpha)N(\beta) \implies N(\alpha) = N(\beta) = 2
  \end{equation}
  But we cannot have \(x^2 + 5y^2 = 2\) for any \(x, y \in \ints\). So 2 is irreducible. The remaining cases can be handled with the same argument
\end{proof}
We deduce that \(2 \cdot 3 = (1 + \sqrt{-5})(1 - \sqrt{-5})\) shows that \(R\) is \underline{not} a UFD. Moreover, \(2, 3, 1 \pm \sqrt{5}\) are irreducible but \underline{not} prime, even though in some rings (in UFDs and Bezout domains) all irreducible elements were prime. For 2, the argument is
\begin{equation}
  2 | (1 + \sqrt{-5})(1 - \sqrt{-5}), 2 \not| (1 \pm \sqrt{-5})
\end{equation}
Remark: Gauss studied this problem at the beginning of the 1800s, and posed the famous problem: if you look at \(\ints[\sqrt{-d}]\) where \(d \geq 1\) is not square, when do we have a PID or UFD? It turns out that for these kinds of rings this is an equivalent condition. Gauss thought that this should only happen for finitely many \(d\), which he wrote down explicitly. In fact, this is true, but it was only proved in the 1960's.

\section{Polynomial Rings (Chapter 9)}

Recall from last semester:
\begin{itemize}

  \item If \(R\) is a commutative ring, \(R[x]\) is an integral domain if and only if \(R\) is an integral domain, since \(R\) is naturally a subring of \(R[x]\) and the leading coefficient of products of elements in \(R[X]\) is just the product of the leading coefficients.

  Assume now \(R\) is an integral domain. Then \(\Deg(fg) = \Deg(f) + \Deg(g)\), i.e. degree is additive. This implies that \(R[x]^\times\) is of degree 0, and is composed of the units of \(R\). Remember that this can fail in general if \(R\) is not a domain.

  In general, we can iterate to define a polynomial ring \(R[x_1,...,x_n]\) by induction to be
  \begin{equation}
    R[x_1,...,x_n] := R[x_1,...,x_{n - 1}][x_n]
  \end{equation}
  Note that if we think about it this way, the polynomials are most naturally written in the form
  \begin{equation}
    (x_1^2 - 1)x_2^3 + (3x_1 + 2)x_2^2 + (x_1)x_2
  \end{equation}
  We can also forget all the parentheses and think about it in a more ``democratic'' way between \(x_1\) and \(x_2\), writing
  \begin{equation}
    R[x_1,...,x_n] = \left\{\text{Finite }
      \sum_{i_1,...,i_n \geq 0}a_{i_1...i_n}x_1^{i_1}...x_n^{i_n} : a_{i_1...i_n} \in R
    \right\}
  \end{equation}
  Now to be very rigorous, we can define the ring both ways and construct an isomorphism between them. But that's just formalism. Note that this gives us the useful ability, however,to think about rings in multiple ways:
  \begin{equation}
    R[x_1, x_n] \simeq R[x_1][x_2] \simeq R[x_2][x_1]
  \end{equation}

\end{itemize}

This week, we'll be proving that \(R\) is a UFD if and only if \(R[x]\) is a UFD, step by step, as on the worksheet.

\begin{enumerate}

  \item Show that \(R\) is a UFD if and only if every nonzero nonunit in \(R\) is a product of prime elements. This can be considered a second way to define a UFD.
  \begin{proof}
    Implication is easy, since in a UFD, an element is prime if and only if it is irreducible, and we have a factorization into irreducibles. For the reverse direction, since in any integral domain prime implies irreducible, once we have factorization into prime elements, we also have a factorization into irreducibles. Now we are left to prove uniqueness: assume
    \begin{equation}
      \pi_1...\pi_n = \pi_1'...\pi_m'
    \end{equation}
    where each \(\pi_i, \pi_i'\) are irreducible. If \(\pi \in R\) is irreducible, by assumption \(\pi = p_1...p_r\) where each \(p_i\) are prime. Since each \(\pi_i, \pi_i'\) is irreducible, \(r = 1\), each \(\pi_i, \pi_i'\) is prime and hence we can show similarity.
  \end{proof}

\end{enumerate}
We'll continue our proof next time.

\end{document}
